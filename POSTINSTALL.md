This app is pre-setup with an admin account. The initial credentials are:

**Email**: admin@cloudron.local<br/>
**Password**: changeme123<br/>

**Please change the admin password on first login**

<sso>
By default, Cloudron users have `Guest` permissions. This can be changed in the Authentication page.
</sso>

